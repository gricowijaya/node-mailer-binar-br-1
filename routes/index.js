const express = require("express");
const router = express.Router();
const restrict = require("../middlewares/restrict");
const controller = require("../controllers");
/*
    endpoint forgot password -> http://localhost/forgot-password {email: user_email}
*/
// ambil user_email
// findOne by email
// generate token (user_id)
// kirim token dan url ganti password ke email user -> http://localhost/reset_password?token=

/*
    endpoint reset password -> http://localhost/reset-password?token=  {new_password}
*/
// ambil token dan body data
// extract/verify token -> dapatin payload { user_id}
// validasi apakah user_id ada di database
// bcrypt(password)
// update user password where user_id = payload.user_id
// success

router.post("/register", controller.register);
router.post("/login", controller.login);
router.get("/whoami", restrict, controller.whoami);
router.post("/forgot-password", controller.forgotPassword);
router.get("/reset-password", controller.viewResetPassword);
router.post("/reset-password", controller.resetPassword);

module.exports = router;
