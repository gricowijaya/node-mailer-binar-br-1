require("dotenv").config();
const express = require("express");
const app = express();
const router = require("./routes");
const { PORT } = process.env;
const bodyParser = require("body-parser");

app.use(bodyParser({ extended: false }));
app.use(express.json());
app.use(router);
app.set("view engine", "ejs");

app.listen(PORT, console.log(`Listen ${PORT}`));
