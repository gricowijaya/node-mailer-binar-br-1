const { User } = require("../db/models");
const googleLibrary = require("../lib/google");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { JWT_SECRET_KEY, HOST, PORT } = process.env;

module.exports = {
  register: async (req, res, next) => {
    try {
      const { name, email, password } = req.body;

      const existUser = await User.findOne({ where: { name: name } });
      if (existUser) {
        return res.status(409).json({
          status: false,
          message: "name already used!",
        });
      }

      const encryptedPassword = await bcrypt.hash(password, 10);

      const user = await User.create({
        name: name,
        email: email,
        password: encryptedPassword,
      });

      console.log(user);

      return res.status(201).json({
        status: true,
        message: "success",
        data: {
          name: user.name,
          email: user.email,
        },
      });
    } catch (err) {
      next(err);
    }
  },

  login: async (req, res, next) => {
    try {
      const { email, password } = req.body;
      const userExist = await User.findOne({ where: { email } });

      if (!userExist) {
        return res.status(404).json({
          status: false,
          message: "user not found!",
          data: null,
        });
      }

      // check password
      const passwordMatch = await bcrypt.compare(password, userExist.password);
      if (!passwordMatch) {
        return res.status(400).json({
          status: false,
          message: "wrong password!",
          data: null,
        });
      }

      // generate token
      const payload = {
        id: userExist.id,
        name: userExist.name,
        email: userExist.email,
      };
      const token = jwt.sign(payload, process.env.JWT_SECRET_KEY);

      // return token
      return res.status(200).json({
        status: true,
        message: "success",
        data: {
          user_id: userExist.id,
          token,
        },
      });
    } catch (err) {
      // console.log(err)
      next(err);
    }
  },

  whoami: (req, res, next) => {
    try {
      const user = req.user;
      return res.status(200).json({
        status: true,
        message: "success",
        data: user,
      });
    } catch (err) {
      next(err);
    }
  },

  forgotPassword: async (req, res, next) => {
    try {
      const { email } = req.body;
      // sequelize query
      const user = await User.findOne({ where: { email: email } });
      const payload = {
        id: user.id,
        name: user.name,
        email: user.email,
      };

      // generate token
      const tokenForgotPassword = jwt.sign(payload, JWT_SECRET_KEY);
      // create url untuk forgot password page, buat request password yang baru
      const forgotPasswordURL = `${HOST}:${PORT}/reset-password?token=${tokenForgotPassword}`;
      // subjectnya
      const subject = `Forgot Password Email Breakout Room 1`;
      // content untuk emailnya
      const data = `click this link to redirect for your forgot password request ${forgotPasswordURL}`;

      const response = await googleLibrary.sendEmail(
        email, // to
        subject, // subject
        data // content
      );

      return res.status(200).json({
        status: true,
        message: "success",
        data: forgotPasswordURL,
      });
    } catch (err) {
      next(err);
    }
  },

  viewResetPassword: async (req, res, next) => {
    try {
      const { token } = req.query;
      const { password } = req.body;
      if (!token) {
        return res.status(401).json({
          status: false,
          message: "you're not authorized!",
          data: null,
        });
      }
      // verify the token
      console.log(token);
      const payload = jwt.verify(token, JWT_SECRET_KEY);
      // not secure
      const userExists = await User.findOne({
        where: { email: payload.email },
      });

      if (!userExists) {
        return res.status(401).json({
          status: false,
          message: "you're email is not registered!, Please register",
          data: null,
        });
      }

      res.render("forgot-password", { userExists, token });
    } catch (err) {
      console.log(err);
      next(err);
    }
  },

  resetPassword: async (req, res, next) => {
    try {
      const { token } = req.query;
      const { password } = req.body;
      // console.log(token);
      // if (!token) {
      //   return res.status(401).json({
      //     status: false,
      //     message: "you're not authorized!",
      //     data: null,
      //   });
      // }
      // verify the token
      const payload = jwt.verify(token, JWT_SECRET_KEY);
      // not secure
      const userExists = await User.findOne({
        where: { email: payload.email },
      });

      if (!userExists) {
        return res.status(401).json({
          status: false,
          message: "you're email is not registered!, Please register",
          data: null,
        });
      }

      const encryptedPassword = await bcrypt.hash(password, 10);

      const update = await User.update(
        { password: encryptedPassword },
        { where: { email: payload.email } }
      );

      return res.status(200).json({
        status: true,
        message: "success",
        data: update,
      });
    } catch (err) {
      console.log(err);
      next(err);
    }
  },
};
